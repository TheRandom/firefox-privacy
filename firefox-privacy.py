#!/usr/bin/python3
"""

Firefox Configuration Python

"""

# Packages for system interaction
from time import sleep
import platform
from os import listdir, getlogin, getenv
# Packages for web scraping
import urllib.request
from selenium import webdriver
import selenium.common.exceptions
from selenium.webdriver.firefox.options import Options
# Packages to manage files
import tarfile
import shutil
import zipfile
# Packages to print with colors
from colorama import Fore
from colorama import Style

###### important vars ######
path = ""

def chooseProfile(number):
	if("Windows" in platform.platform()):
		path="c:/users/"+getlogin()+"/appdata/roaming/mozilla/firefox/profiles/"+profiles[number]+"/prefs.js"
	else:
		path="/home/"+getlogin()+"/.mozilla/firefox/"+profiles[number]+"/prefs.js"
	return path

def download_geckodriver(url, filename):
	urllib.request.urlretrieve(url, filename)

def scrap(url):
	# Define headless mode
	options = Options()
	options.headless = True
	# Create browser
	browser = webdriver.Firefox(options=options, executable_path=r'geckodriver')
	# Default time to wait
	browser.implicitly_wait(5)
	# Open web page
	browser.get(url)
	# Our list of configs
	configs = {}
	# Scrap parameters
	i = 1
	# For each element
	while True:
		# Try to find them
		try:
			param = browser.find_element_by_xpath('/html/body/div/main/dl/dt[' + str(i) + ']')
			i = i + 1
			if '=' in param.text:
				configs[param.text.split(' = ')[0]] = param.text.split(' = ')[1]
				print(f"[{Fore.GREEN}+{Style.RESET_ALL}] Found parameter: ", param.text)
			elif 'Disable Firefox prefetching pages it thinks you will visit next:' == param.text:
				j = 1
				while True:
					try:
						param = browser.find_element_by_xpath('/html/body/div/main/dl/dd[19]/ul/li['+ str(i) +']')
						j = j + 1
						print(f"[{Fore.GREEN}+{Style.RESET_ALL}] Found parameter: ", param.text)
					except selenium.common.exceptions.NoSuchElementException:
						print(f"[{Fore.YELLOW}!{Style.RESET_ALL}] End of prefetching parameters list !")
						break

			else:
				print(f"[{Fore.YELLOW}?{Style.RESET_ALL}] Don't know what to do with :", param.text)
		# If end of list
		except selenium.common.exceptions.NoSuchElementException:
			print(f"[{Fore.YELLOW}!{Style.RESET_ALL}] End of parameters list !")
			break
	print(f"[{Fore.BLUE}*{Style.RESET_ALL}] Add WebRTC parameters ...")
	i = 1
	while True:
		try:
			param = browser.find_element_by_xpath('/html/body/div/main/ol[2]/li[' + str(i) + ']')
			i = i + 1
			if '=' in param.text:
				print(f"[{Fore.GREEN}+{Style.RESET_ALL}] Found parameter: ", param.text)
				configs[param.text.split(' = ')[0]] = param.text.split(' = ')[1]
			else:
				print(f"[{Fore.YELLOW}?{Style.RESET_ALL}] Don't know what to do for this WebRTC parameter:",param.text)
		except selenium.common.exceptions.NoSuchElementException:
			print(f"[{Fore.YELLOW}!{Style.RESET_ALL}] End of WebRTC parameters list")
			break

	# Close browser
	browser.close()
	# Return our dictionary with configs
	return configs
##### configuration of prefs.js #####

def config(path, configs):
	print(f"[{Fore.BLUE}*{Style.RESET_ALL}] Reading default user_pref file ...")
	with open(path) as file:
		data = file.read()
	file.closed
	print(f"[{Fore.BLUE}*{Style.RESET_ALL}] Writing config in file ...")
	with open(path, 'w') as file:
		for param in configs:
			config_line = "user_pref(\"" + param + "\"," + configs[param] + ");"
			file.write(config_line + "\n")
			print(f"[{Fore.GREEN}+{Style.RESET_ALL}] Writed: " + config_line)
	file.closed
	print(f"[{Fore.BLUE}*{Style.RESET_ALL}] Done")



######### Main ########
if __name__ == "__main__":
	print(f"[{Fore.BLUE}*{Style.RESET_ALL}] Start scraping ...")
	configs = scrap('https://privacytools.io/browsers/')
	###### profile from os ######
	if("Windows" in platform.platform()):
		profiles=listdir("c:\\users\\"+getlogin()+"\\appdata\\roaming\\mozilla\\firefox\\profiles\\")
		download_geckodriver('https://github.com/mozilla/geckodriver/releases/download/v0.29.1/geckodriver-v0.29.1-win64.zip', 
			'geckodriver.zip')
		file = zipfile.ZipFile('geckodriver.zip', 'r')
		file.extractall('.')
		shutil.copyfile('geckodriver.exe', 'geckodriver')
	else:
		profiles=listdir("/home/"+getlogin()+"/.mozilla/firefox/")
		download_geckodriver('https://github.com/mozilla/geckodriver/releases/download/v0.29.1/geckodriver-v0.29.1-linux64.tar.gz', 
			'geckodriver.tar.gz')
		tar = tarfile.open('geckodriver.tar.gz', "r:gz")
		tar.extractall()
		tar.close()
	print(f"[{Fore.BLUE}*{Style.RESET_ALL}] Choose Profile")
	for i in range(0,len(profiles)):
		print("\t"+str(i)+") "+profiles[i])
	path = chooseProfile(int(input("> ")))
	print(f"[{Fore.BLUE}*{Style.RESET_ALL}] Path of profile is:", path)
	config(path, configs)
	print(f"[{Fore.BLUE}*{Style.RESET_ALL}] Exiting ...")
	sleep(2)
