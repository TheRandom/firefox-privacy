# Firefox-Privacy

This script can be used to update configurations of firefox. Configurations and parameters come from the website https://www.privacytools.io/
This script has been made with python 3.

# Manual for Windows
- Install python 3
- Execute the script
- Choose profile you want to modify
- Enjoy :-)

# Manual for Linux
- Launch the python script (python3 firefox-privacy.py) in the local directory
- Choose the profile you want to modify
- Enjoy :-)

# Future update
Installing useful addons automatically.

# Legal purpose
You can use, modify and share the script but try to give your sources ;-)
